The 6502 cheat sheet is an HTML page, dynamically created from a sqlite3 database, containing information about opcodes. 
It has been made to print this cheat sheet on various media such as mugs and posters.
It uses php-crud-api to connect to database and fetch necessary data to build the sheet. 

Feel free to use and/or modify it to suit your needs.