const uri = "https://mimuma.pl/opcodes/api.php/records/"
const requestOptions = {method: 'GET',redirect: 'follow'};
let targetcolumn = 1;
const cdiv = (cname,content) => $('<div/>').addClass(cname).html(content);
const adiv = divarr => Object.entries(divarr).map(([k,v]) => cdiv(k,v));
const addBlock = txt =>  $(`#col${targetcolumn}`).append(cdiv('iblock',txt));
const addHeader = txt => $(`#col${targetcolumn}`).append(cdiv('iheader',txt));
const addSpacer = h => $(`#col${targetcolumn}`).append(cdiv('spacer','&nbsp;').css('height',h|10));

const createOpcode = opc => {
    if (opc.type.id>1) {
        opc.opmodes = [{hex:opc.hex,time:opc.time,mode:{name:'IMM',len:(opc.type.id==2)?2:1}}];
    }
    if (opc.desc == 'transfer X to SP') opc.desc = 'transfer X<br>to SP'
    if (opc.desc == 'transfer SP to X') opc.desc = 'transfer SP<br>to X'
    if (opc.desc == 'branch on not equal') opc.desc = 'branch on<br>not equal'
    let odesc = {'mnemonic':opc.mnemonic, 'flags':opc.flags, 'desc':opc.desc}
    if (opc.flags=='B') {
        opc.flags='B (on stack)';
        odesc = {'mnemonic':opc.mnemonic, 'flags letters0':opc.flags, 'desc':opc.desc};
    } 
    if (opc.flags=='All') {
        opc.flags='All (from stack)';
        odesc = {'mnemonic':opc.mnemonic, 'flags letters0':opc.flags, 'desc':opc.desc};
    } 

    const opblock =  cdiv('opblock').append(...adiv(odesc));
    const ahdr = $("<li/>").addClass('aheader')
        .append(...adiv({'aname':'name', 'hex':'hex', 'len':'len','time':'time'}));
    const addr =  $("<ul/>").addClass('addrlist').append(ahdr).appendTo(opblock);
    opc.opmodes.forEach(a => { addr.append(
        $("<li/>").append(...adiv({'aname':a.mode.name, 'hex': `$${a.hex}`, 'len':a.mode.len, 'time':a.time}))
    )});
    return opblock
} 

const getOpcodesByType = async type => {
    const response = await fetch(`${uri}opcodes?filter=type,eq,${type}&join=optypes&join=opmodes,addrmodes&order=type,asc`, requestOptions);
    return (await response.json()).records;
}

const drawOpcodes = async (type, breaklist) => {
    let opcodes = await getOpcodesByType(type)
    for (opcode of opcodes) {
        $(`#col${targetcolumn}`).append(createOpcode(opcode))
        if (Array.isArray(breaklist)) {
            if (breaklist.includes(opcode.mnemonic)) targetcolumn++;
        }
    }
}


const buildSite = async () => {
    
    await drawOpcodes(1,['CPY','LDY','SBC']);
    addSpacer()
       
    addHeader('Branch Instructions:');
    await drawOpcodes(2)
    
    targetcolumn++;
    addHeader('Flag Instructions:');
    await drawOpcodes(3)
    
    addSpacer(20)
    addHeader('Stack Instructions:');
    await drawOpcodes(5)

    targetcolumn++
    addHeader('Register Instructions:');
    await drawOpcodes(4)

    addSpacer()
    addHeader('6502 Registers:');
    addBlock(
    '<div class="notice">SP - Stack pointer</div>'+
    '<div class="notice">PC - Program Counter</div>'+
    '<div class="notice">X - Index register</div>'+
    '<div class="notice">Y - Index register</div>'+
    '<div class="notice">A - Accumulator</div>'+
    '<div class="notice">F - Flag Register (Processor Status)</div>'+
    '<div class="rbox">N</div>'+
    '<div class="rbox">V</div>'+
    '<div class="rbox">*</div>'+
    '<div class="rbox">B</div>'+
    '<div class="rbox">D</div>'+
    '<div class="rbox">I</div>'+
    '<div class="rbox">Z</div>'+
    '<div class="rbox">C</div>'
    )
    
    addSpacer(15)
    addHeader('Branch execution time:');
    addBlock(
    'A branch that is not taken requires two machine cycles. Add one if the branch is taken and add another if the branch crosses the page boundary.'
    );    

    addSpacer(15)
    addHeader('Vectors:');
    addBlock(
        '<div class="notice">$FFFA - NMI</div>'+        
        '<div class="notice">$FFFC - RESET</div>'+        
        '<div class="notice">$FFFE - IRQ</div>'
    );    

    //

    


}

(function($) {
	$.fn.styleFirstWord = function(attr) {
		if ( typeof attr !== 'object' ) {
			attr = {
				class: attr || 'firstWord'
			};
		}

		return this.each(function() {
			var $this = $(this),
					headingText = $this.text().split(' '),
					span = $('<span>', attr).text(headingText.shift());

			$this.html(
				$('<div>').append(span).html() + ' ' + headingText.join(' ')
			);
		});
	};
})(jQuery);

$(document).ready(function() {
    buildSite().then(()=>{
        $('.notice').styleFirstWord('bold')
    });


    
});
  